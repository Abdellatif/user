package com.mar.manage.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.user.model.Member;

public interface MemberRepository extends JpaRepository<Member, Long> {
	public Optional<Member> findByLogin(String login);
}
