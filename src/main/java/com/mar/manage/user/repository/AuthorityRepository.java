package com.mar.manage.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mar.manage.user.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
