package com.mar.manage.user.exception;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class UserResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	// All Exceptions
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleException(Exception ex) {
		return new ResponseEntity<>(new String("Exception occurred"),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	// MemberNotFoundException Exception
	@ExceptionHandler(MemberNotFoundException.class)
	public final ResponseEntity<Object> handleMemberNotFoundException(MemberNotFoundException ex) {
	    
		return new ResponseEntity<>(new String("Member Not Found"),
				HttpStatus.NOT_FOUND);
	}
	
	// AuthorityNotFoundException Exception
	@ExceptionHandler(AuthorityNotFoundException.class)
	public final ResponseEntity<Object> handleAuthorityNotFoundException(AuthorityNotFoundException ex) {
	    
		return new ResponseEntity<>(new String("Authority Not Found"),
				HttpStatus.NOT_FOUND);
	}
	
	// DataIntegrityViolationException Exception : for SQL exceptions
	@ExceptionHandler(DataIntegrityViolationException.class)
	public final ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
		return new ResponseEntity<>(new String("SQL : violation of an integrity constraint"),
				HttpStatus.BAD_REQUEST);
	}
	
	// DataIntegrityViolationException Exception : for SQL exceptions
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
		return new ResponseEntity<>(new String("SQL : violation of a defined integrity constraint"),
				HttpStatus.BAD_REQUEST);
	}
	
	// ModelNotValideException Exception : for models validation in Business layer
	@ExceptionHandler(ModelNotValideException.class)
	public final ResponseEntity<Object> handleModelNotValideException(ModelNotValideException ex) {
		return new ResponseEntity<>(new String("Model not valide"),
				HttpStatus.BAD_REQUEST);
	}
	
	// MethodArgumentNotValidException Exception : for not valid models
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    			HttpHeaders headers,
            	HttpStatus status,
            	WebRequest request) {
		
        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);

    }
}
